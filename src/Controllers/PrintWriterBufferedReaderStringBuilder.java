package Controllers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class PrintWriterBufferedReaderStringBuilder {

	public static void main(String[] args) throws IOException {
		String jrePath = System.getProperty("java.home");
		String userPath = System.getProperty("user.home");
		String fileSeparator = System.getProperty("file.separator");
		// String lineSeparator = System.getProperty("line.separator");

		BufferedReader bufferedReader = null;
		PrintWriter printWriter = null;
		PrintWriter printWriterBroken = null;
		
		File inputFile = new File(jrePath + fileSeparator + "Input.txt");
		File outputFile = new File(userPath + fileSeparator + "desktop" + fileSeparator + "printOutput.csv");
		File brokenFile = new File(userPath + fileSeparator + "desktop" + fileSeparator + "printOutputBroken.csv");

		try {
			bufferedReader = new BufferedReader(new FileReader(inputFile));
			printWriter = new PrintWriter(new FileWriter(outputFile));
			printWriterBroken = new PrintWriter(new FileWriter(brokenFile));

			//still trying to read from file without creating String object each time on loop
			//but the below commented code is still incomplete and is wrong
			
//			StringBuilder stringBuilder = new StringBuilder();
//			while((stringBuilder.replace(0,stringBuilder.length(), bufferedReader.readLine())) != null){
//				System.out.println(stringBuilder.toString());
//			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// try catch for try block
			// throws IOException in method for finally block
			bufferedReader.close();
			printWriter.flush();
			printWriterBroken.flush();
			printWriter.close();
			printWriterBroken.close();
		}


	}

}
