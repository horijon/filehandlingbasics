package Controllers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileReaderUsingBufferedReader {

	public static void main(String[] args) throws IOException {
		String jrePath = System.getProperty("java.home");
		String userPath = System.getProperty("user.home"); 
		String fileSeparator = System.getProperty("file.separator");
		// String lineSeparator = System.getProperty("line.separator");

		BufferedReader bufferedReader = null;
		BufferedWriter bufferedWriter = null;
		BufferedWriter brokenWriter = null;
		
		File inputFile = new File(jrePath + fileSeparator + "Input.txt");
		File outputFile = new File(userPath + fileSeparator + "desktop" + fileSeparator + "Output.csv");
		File brokenFile = new File(userPath + fileSeparator + "desktop" + fileSeparator + "Broken.csv");
		
		try {
			bufferedReader = new BufferedReader(new FileReader(inputFile));
			bufferedWriter = new BufferedWriter(new FileWriter(outputFile));
			brokenWriter = new BufferedWriter(new FileWriter(brokenFile));
			
			String string = null;
			while ((string = bufferedReader.readLine()) != null) {

				string = string.trim();
				String[] s = string.split("[^\\S]+");
					if (s.length == 4) {
						bufferedWriter.write(string.replaceAll("[^\\S]+", ","));
						bufferedWriter.newLine();
					} else {
						brokenWriter.write(string.replaceAll("[^\\S]+", ","));
						brokenWriter.newLine();
					}

			}

		} catch (IOException e) {
			System.out.println("The file may have been opened from outside java");
			e.printStackTrace();
		} finally {
			bufferedReader.close();
			bufferedWriter.flush();
			brokenWriter.flush();
			bufferedWriter.close();
			brokenWriter.close();
		}
	}

}
