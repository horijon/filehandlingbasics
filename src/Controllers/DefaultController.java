package Controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class DefaultController {

	public static void main(String[] args) throws IOException {
		System.out.println("Hello World");
		String jrePath = System.getProperty("java.home");
		FileInputStream in = null;
		FileOutputStream out = null;
		File file = null;
		try {
			in = new FileInputStream(jrePath + "\\Input.txt");
			file = new File("C:\\Users\\horijon\\Desktop\\" + "Output.txt");
			
			
			if (file.createNewFile()){
				System.out.println("File is created!");
			}else{
				System.out.println("File already exists.");
			}
			
			
			out = new FileOutputStream(file);
					// by default the second parameter is false for the other constructor of FileOutputStream
					// if the second parameter is false then overwrite, if true then append
			
			int c;
			while((c = in.read()) != -1){
				out.write(c);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			in.close();
			out.flush();
			out.close();
		}

	}

}
